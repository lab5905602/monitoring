class Counter {
    constructor(initialValue = 0) {
        this.value = initialValue;
        this.count = 0;
    }

    increase() {
        this.value++;
        this.count = this.value;
    }

    zero() {
        this.value = 0;
        this.count = this.value;
    }

    read() {
        return this.value;
    }
}

export default Counter;
